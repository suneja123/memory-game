const gameContainer = document.getElementById("game");

const gif = [
  "gifs/1.gif",
  "gifs/2.gif",
  "gifs/3.gif",
  "gifs/4.gif",
  "gifs/5.gif",
  "gifs/6.gif",
  "gifs/7.gif",
  "gifs/8.gif",
  "gifs/9.gif",
  "gifs/10.gif",
  "gifs/1.gif",
  "gifs/2.gif",
  "gifs/3.gif",
  "gifs/4.gif",
  "gifs/5.gif",
  "gifs/6.gif",
  "gifs/7.gif",
  "gifs/8.gif",
  "gifs/9.gif",
  "gifs/10.gif"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledGif = shuffle(gif);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(gif) {
  for (let i = 0; i < gif.length; i++) {
    // create a new div
    const newDiv = document.createElement("img");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif[i]);
    newDiv.setAttribute("src", "gifs/mill.gif");

    newDiv.setAttribute("id", i);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}
let openCards = [];
let count = 0;
let move = 0;
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked


  console.log("hello");
  if (openCards.length < 2) {

  let gif = event.target.classList[0];

  console.log(event.target, gif);

  event.target.classList.toggle("myDiv");
  event.target.setAttribute("src", gif);
    if (move > 30) {
      let localScore = localStorage.getItem("score3");
      if (localScore < count && localScore != 0) {
        // newDiv.innerHTML = "new score is " + count;

        localStorage.setItem("score3", count);
        console.log(localStorage);
      }
      localScore = localStorage.getItem("score3");

      if (move == 31) {
        const newDiv = document.createElement("div");
        newDiv.classList.add("time");
        if (count <= 10) {
          newDiv.innerHTML = "times up your score is " + count;
        } else {
          newDiv.innerHTML = `You win the level 3 <br> your current score ${count}<br> your High score is ${localScore} `;
          newDiv.append(btn);
        }
        gameContainer.append(newDiv);
        move++;
      }

      return;
    } else {
      move++;
    }
    openCards.push(event);
    if (openCards.length === 2) {
      console.log(
        "name",
        openCards[0].target.className,
        openCards[1].target.className
      );
      if (
        openCards[0].target.className === openCards[1].target.className &&
        openCards[0].target.getAttribute("id") !==
          openCards[1].target.getAttribute("id")
      ) {
        count++;
        openCards = [];
        console.log("matched");

        let div1 = document.getElementById(
          `${openCards[0].target.getAttribute("id")}`
        );
        let div2 = document.getElementById(
          `${openCards[1].target.getAttribute("id")}`
        );
        div1.setAttribute("src", "gifs/mill.gif");
        div2.setAttribute("src", "gifs/mill.gif");
      } else if (
        openCards[0].target.getAttribute("id") ===
        openCards[1].target.getAttribute("id")
      ) {
        openCards.pop();
        move--;
      } else {
        console.log("not matched");

        setTimeout(() => {
          let div1 = document.getElementById(
            `${openCards[0].target.getAttribute("id")}`
          );
          let div2 = document.getElementById(
            `${openCards[1].target.getAttribute("id")}`
          );
          div1.classList.toggle("myDiv");
          div1.setAttribute("src", "gifs/mill.gif");
          div2.classList.toggle("myDiv");
          div2.setAttribute("src", "gifs/mill.gif");

          //  div1.classList.remove("miDiv");
          // div1.classList.remove("myDiv");

          openCards = [];
        }, 1000);
      }
    }
    const counter = document.getElementsByClassName("counter")[0];
    counter.innerHTML = "moves "+move;
    const score = document.getElementsByClassName("score")[0];
    score.innerHTML = "score "+count;
  }
}

// when the DOM loads
createDivsForColors(shuffledGif);
