const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let i = 0; i < colorArray.length; i++) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(colorArray[i]);
    newDiv.setAttribute("id", i);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}
let openCards = [];
let count = 0;
let move = 0;
let checkMatchCard = [];
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked

  if (
    openCards.length < 2 &&
    !checkMatchCard.includes(event.target.getAttribute("id"))
  ) {
    if (move > 10) {
      let localScore = localStorage.getItem("score");
      if (localScore < count && localScore != 0) {
        // newDiv.innerHTML = "new score is " + count;

        localStorage.setItem("score", count);
        console.log(localStorage);
      }
      localScore = localStorage.getItem("score");

      if (move == 11) {
        const newDiv = document.createElement("div");
        newDiv.classList.add("time");
        // const count=localStorage.getItem("score");
        console.log(count);
        if (count < 2) {
          newDiv.innerHTML = "times up your score is " + count;
        } else {
          newDiv.innerHTML = `You win the level 1 <br> your current score ${count}<br> your High score is ${localScore} `;
          const btn = document.createElement("button");
          const a = document.createElement("a");
          a.setAttribute("href", "index1.html");
          a.innerHTML = "Next Level";
          btn.append(a);
          newDiv.append(btn);
        }
        gameContainer.append(newDiv);
        move++;
      }

      return;
    } else {
      move++;
    }
    let color = event.target.classList.value;
    event.target.classList.toggle("myDiv");

    event.target.style["background-color"] = color;
    console.log(event.target);
    // event.target.getAttribute("id")
    openCards.push(event);
    if (openCards.length === 2) {
      if (
        openCards[0].target.className === openCards[1].target.className &&
        openCards[0].target.getAttribute("id") !==
          openCards[1].target.getAttribute("id")
      ) {
        count++;
        checkMatchCard.push(openCards[0].target.getAttribute("id"));
        checkMatchCard.push(openCards[1].target.getAttribute("id"));
        console.log(checkMatchCard);
        openCards = [];
        console.log("matched");
      } else if (
        openCards[0].target.getAttribute("id") ===
        openCards[1].target.getAttribute("id")
      ) {
        openCards.pop();
        move--;
      } else {
        console.log("not matched");
        setTimeout(() => {
          let div1 = document.getElementById(
            `${openCards[0].target.getAttribute("id")}`
          );
          let div2 = document.getElementById(
            `${openCards[1].target.getAttribute("id")}`
          );

          console.log(div1, div2);
          div1.classList.toggle("myDiv");
          div1.style["background-color"] = "white" ;
          div2.classList.toggle("myDiv");
          div2.style["background-color"] = "white";

          openCards = [];
        }, 1000);
      }
    }
    const counter = document.getElementsByClassName("counter")[0];
    counter.innerHTML = "moves :" +move;
    const score = document.getElementsByClassName("score")[0];
    score.innerHTML = "score "+count;
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);
function startGame() {
  document.querySelector(".backDrop").style.display = "none";
  document.getElementById("game").removeChild();
  createDivsForColors(shuffledColors);
  document.querySelector("#game").style.left="0";
}